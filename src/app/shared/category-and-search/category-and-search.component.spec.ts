import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CategoryAndSearchComponent } from './category-and-search.component';

describe('CategoryAndSearchComponent', () => {
  let component: CategoryAndSearchComponent;
  let fixture: ComponentFixture<CategoryAndSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CategoryAndSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CategoryAndSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
