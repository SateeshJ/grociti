import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';
@Component({
  selector: 'app-user-navigation',
  templateUrl: './user-navigation.component.html',
  styleUrls: ['./user-navigation.component.scss']
})
export class UserNavigationComponent implements OnInit {
  customerDetailsArr: any = [];
  storeName: any;
  currentUser: any;
  ordersObj: any;
  totalOrders: any;
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    public auth: AuthService,
    private spinner: NgxSpinnerService
  ) {
    this.currentUser = JSON.parse(localStorage.getItem('currentUserProfile'));
  }
  ngOnInit() {
    this.storeName = localStorage.getItem('storeName');
    // this.auth.user$ = JSON.parse(localStorage.getItem('currentUserProfile'));
    const data = {
      store_name: this.storeName
    };
    // this.spinner.show();
    this.store.getAllCustomerOrdersByEmailID(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          this.ordersObj = res.message;
          this.totalOrders = Object.keys(this.ordersObj).length;
          // this.customerGroceriesArr = [];
          //this.ordersObj = Object["values"](res.message);
          // this.ordersTotal = 0;

          // Object.keys(this.ordersObj).forEach(key => {
          //   this.ordersTotal =
          //     this.ordersTotal + +this.ordersObj[key].order.payable;
          //   this.customerGroceriesArr.push(this.ordersObj[key]);
          // });

          // this.customerGroceriesArr.reverse();

          // this.spinner.hide();
        } else {
          // this.customerGroceriesArr = [];
          // this.spinner.hide();
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}
