import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageService } from '@app/services/storage.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  searchInput: any;
  constructor(private router: Router, private storageService: StorageService) {}

  ngOnInit() {}
  onSubmit() {
    this.router.navigate(['/search', { input: this.searchInput }]);
  }
}
