import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-subscription-dialog',
  templateUrl: './subscription-dialog.component.html',
  styleUrls: ['./subscription-dialog.component.css']
})
export class SubscriptionDialogComponent implements OnInit {
  subscriptionData: any = [];
  fromDialog: string;

  constructor(
    public dialogRef: MatDialogRef<SubscriptionDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.subscriptionData = data.pageValue;
    console.log(this.subscriptionData, 'aflkndsl');
  }
  ngOnInit() {}
}
