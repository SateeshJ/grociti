import { Component, OnInit, Inject } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Title, Meta } from '@angular/platform-browser';

import swal from 'sweetalert2';
@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss']
})

export class SubscribeComponent implements OnInit {

  title = 'Not Jus A` Salad | MEAL PLAN REGISTRATION';
  plansArr: any = {
    'custom': {
      name: 'Custom',
      price: '1500',
      currency: 'JMD',
      display_price: 'JMD $1500/day'
    },
    '5-day': {
      name: '5-Day',
      price: '5000',
      currency: 'JMD',
      display_price: 'JMD $5000'
    },
    '4-week': {
      name: '4-week',
      price: '24000',
      currency: 'JMD',
      display_price: 'JMD $24000'
    }
  };
  mealsMenuArr: any = {
    monday: {
      category: '',
      products: ''
    },
    tuesday: {
      category: '',
      products: ''
    },
    wednesday: {
      category: '',
      products: ''
    },
    thursday: {
      category: '',
      products: ''
    },
    friday: {
      category: '',
      products: ''
    },
    friday_special: {
      category: '',
      products: ''
    },
    menu_flag_count: 0
  };
  subscriptionData = {
    plan: '',

    monday: '',
    tuesday: '',
    wednesday: '',
    thursday: '',
    friday: '',
    friday_special: '',

    items: {
      monday: {
        day: ' ',
        item: ' '
      },
      tuesday: {
        day: ' ',
        item: ' '
      },
      wednesday: {
        day: ' ',
        item: ' '
      },
      thursday: {
        day: ' ',
        item: ' '
      },
      friday: {
        day: ' ',
        item: ' '
      },
      friday_special: {
        day: ' ',
        item: ' '
      }
    },

    booking_days: '',
    booking_meals: '',
    booking_meals_items: '',
    meals_menu: '',
    total: '',
    plan_details: '',
    allergies: '',
    store: '',
    customer_info: {
      name: '',
      email: ''
    }
  };
  storeArr: any;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    private toastr: ToastrService,
    public router: Router,
    private titleService: Title,
    private metaService: Meta
  ) {
    this.subscriptionData.friday_special = 'none';
  }

  ngOnInit() {
    this.titleService.setTitle(this.title);
    this.metaService.addTags([
      {
        name: 'keywords',
        content:
          'organic,food,marketing,shop,product,organic food,farmer,agriculture,fruits,vegetables,healthy food,resposnsive,Fruits Shop, vegetable store, Ecommerce, Stores, Orders, Payments, Groceries, Clothings, Shopping'
      },
      {
        name: 'description',
        content: `Clean, Healthy Lunches Prepared Fresh and Delivered to You Daily (Free Delivery). The Cleanest, Tastiest and Most Diverse Healthy Eating Experience! We're looking forward to having you on board :)`
      },
      { name: 'robots', content: 'index, follow' }
    ]);

    this.localStorage.removeItem('subscriptionData');
    this.getStoreDetails();
  }

  onClickSubmit(formData: any) {
    this.spinner.show();
    //alert("hi");
    //console.log(formData.value);
    // console.table(this.subscriptionData);

    if (
      !this.subscriptionData.plan ||
      this.subscriptionData.plan === 'none' ||
      this.subscriptionData.plan === ''
    ) {
      this.toastr.warning('Choose subscription plan!');
      this.spinner.hide();
      return;
    }
    let bookingDays: any;
    let bookingMeals: any;
    let bookingMealsItems: any;
    switch (this.subscriptionData.plan.toLowerCase().trim()) {
      case 'custom': {
        bookingDays = [];
        bookingMeals = [];
        bookingMealsItems = [];
        if (
          this.subscriptionData.monday &&
          this.subscriptionData.monday !== 'none' &&
          this.subscriptionData.monday !== ''
        ) {
          bookingDays.push('Monday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.monday].name
          );

          let mealItemList = {
            day: 'Monday',
            item: this.storeArr.products[this.subscriptionData.monday]
          };

          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.monday = mealItemList;
        }
        if (
          this.subscriptionData.tuesday &&
          this.subscriptionData.tuesday !== 'none' &&
          this.subscriptionData.tuesday !== ''
        ) {
          bookingDays.push('Tuesday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.tuesday].name
          );

          let mealItemList = {
            day: 'Tuesday',
            item: this.storeArr.products[this.subscriptionData.tuesday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.tuesday = mealItemList;
        }
        if (
          this.subscriptionData.wednesday &&
          this.subscriptionData.wednesday !== 'none' &&
          this.subscriptionData.wednesday !== ''
        ) {
          bookingDays.push('Wednesday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.wednesday].name
          );

          let mealItemList = {
            day: 'Wednesday',
            item: this.storeArr.products[this.subscriptionData.wednesday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.wednesday = mealItemList;
        }
        if (
          this.subscriptionData.thursday &&
          this.subscriptionData.thursday !== 'none' &&
          this.subscriptionData.thursday !== ''
        ) {
          bookingDays.push('Thursday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.thursday].name
          );

          let mealItemList = {
            day: 'Thursday',
            item: this.storeArr.products[this.subscriptionData.thursday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.thursday = mealItemList;
        }
        if (
          this.subscriptionData.friday &&
          this.subscriptionData.friday !== 'none' &&
          this.subscriptionData.friday !== ''
        ) {
          bookingDays.push('Friday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.friday].name
          );

          let mealItemList = {
            day: 'Friday',
            item: this.storeArr.products[this.subscriptionData.friday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.friday = mealItemList;
        }

        if (bookingDays.length === 0) {
          this.toastr.error('Please pick meals for Custom Package Or Change the plan.');
          // this.toastr.info("Picked days - " + bookingDays.join(","));
          this.spinner.hide();
          return;
        }


        //custom package
        if (bookingDays.length !== 5) {


          if (
            !this.subscriptionData.monday ||
            this.subscriptionData.monday === ''
          ) {
            this.toastr.error('Please Choose Meal or None for Other Days.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          }



          if (
            !this.subscriptionData.tuesday ||
            this.subscriptionData.tuesday === ''
          ) {
            this.toastr.error('Please Choose Meal or None for Other Days.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          }



          if (
            !this.subscriptionData.wednesday ||
            this.subscriptionData.wednesday === ''
          ) {
            this.toastr.error('Please Choose Meal or None for Other Days.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          }


          if (
            !this.subscriptionData.thursday ||
            this.subscriptionData.thursday === ''
          ) {
            this.toastr.error('Please Choose Meal or None for Other Days.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          }



          if (
            !this.subscriptionData.friday ||
            this.subscriptionData.friday === ''
          ) {
            this.toastr.error('Please Choose Meal or None for Other Days.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          }




        }
        //custom package

        if (bookingDays.includes('Friday')) {
          if (
            !this.subscriptionData.friday_special ||
            this.subscriptionData.friday_special === ''
          ) {
            this.toastr.error('Please pick dessert or fruit bowl for friday.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          } else {
            if (
              this.subscriptionData.friday_special &&
              this.subscriptionData.friday_special !== 'none' &&
              this.subscriptionData.friday_special !== ''
            ) {
              //  bookingDays.push("Friday Special");
              bookingMeals.push(
                this.storeArr.products[this.subscriptionData.friday_special]
                  .name
              );

              let mealItemList = {
                day: 'Friday Special',
                item: this.storeArr.products[
                  this.subscriptionData.friday_special
                ]
              };
              bookingMealsItems.push(mealItemList);
              this.subscriptionData.items.friday_special = mealItemList;
            }

            // bookingMeals.push(this.subscriptionData.friday_special);
          }
        }

        break;
      }
      case '5-day': {
        bookingDays = [];
        bookingMeals = [];
        bookingMealsItems = [];
        if (
          this.subscriptionData.monday &&
          this.subscriptionData.monday !== 'none' &&
          this.subscriptionData.monday !== ''
        ) {
          bookingDays.push('Monday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.monday].name
          );

          let mealItemList = {
            day: 'Monday',
            item: this.storeArr.products[this.subscriptionData.monday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.monday = mealItemList;
        }
        if (
          this.subscriptionData.tuesday &&
          this.subscriptionData.tuesday !== 'none' &&
          this.subscriptionData.tuesday !== ''
        ) {
          bookingDays.push('Tuesday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.tuesday].name
          );

          let mealItemList = {
            day: 'Tuesday',
            item: this.storeArr.products[this.subscriptionData.tuesday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.tuesday = mealItemList;
        }
        if (
          this.subscriptionData.wednesday &&
          this.subscriptionData.wednesday !== 'none' &&
          this.subscriptionData.wednesday !== ''
        ) {
          bookingDays.push('Wednesday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.wednesday].name
          );

          let mealItemList = {
            day: 'Wednesday',
            item: this.storeArr.products[this.subscriptionData.wednesday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.wednesday = mealItemList;
        }
        if (
          this.subscriptionData.thursday &&
          this.subscriptionData.thursday !== 'none' &&
          this.subscriptionData.thursday !== ''
        ) {
          bookingDays.push('Thursday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.thursday].name
          );

          let mealItemList = {
            day: 'Thursday',
            item: this.storeArr.products[this.subscriptionData.thursday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.thursday = mealItemList;
        }
        if (
          this.subscriptionData.friday &&
          this.subscriptionData.friday !== 'none' &&
          this.subscriptionData.friday !== ''
        ) {
          bookingDays.push('Friday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.friday].name
          );

          let mealItemList = {
            day: 'Friday',
            item: this.storeArr.products[this.subscriptionData.friday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.friday = mealItemList;
        }

        if (bookingDays.length === 0) {
          this.toastr.error('Please pick meals for 5-days Or Change the plan.');
          // this.toastr.info("Picked days - " + bookingDays.join(","));
          this.spinner.hide();
          return;
        }

        if (bookingDays.length !== 5) {
          this.toastr.error('Please pick meals for 5-days Or Change the plan.');
          this.toastr.info('Picked days - ' + bookingDays.join(','));
          this.spinner.hide();
          return;
        }

        if (bookingDays.includes('Friday')) {
          if (
            !this.subscriptionData.friday_special ||
            this.subscriptionData.friday_special === ''
          ) {
            this.toastr.error('Please pick dessert or fruit bowl for friday.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          } else {
            if (
              this.subscriptionData.friday_special &&
              this.subscriptionData.friday_special !== 'none' &&
              this.subscriptionData.friday_special !== ''
            ) {
              //  bookingDays.push("Friday Special");
              bookingMeals.push(
                this.storeArr.products[this.subscriptionData.friday_special]
                  .name
              );

              let mealItemList = {
                day: 'Friday Special',
                item: this.storeArr.products[
                  this.subscriptionData.friday_special
                ]
              };
              bookingMealsItems.push(mealItemList);
              this.subscriptionData.items.friday_special = mealItemList;
            }
          }
        }

        break;
      }
      case '4-week': {
        bookingDays = [];
        bookingMeals = [];
        bookingMealsItems = [];
        if (
          this.subscriptionData.monday &&
          this.subscriptionData.monday !== 'none' &&
          this.subscriptionData.monday !== ''
        ) {
          bookingDays.push('Monday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.monday].name
          );

          let mealItemList = {
            day: 'Monday',
            item: this.storeArr.products[this.subscriptionData.monday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.monday = mealItemList;
        }
        if (
          this.subscriptionData.tuesday &&
          this.subscriptionData.tuesday !== 'none' &&
          this.subscriptionData.tuesday !== ''
        ) {
          bookingDays.push('Tuesday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.tuesday].name
          );

          let mealItemList = {
            day: 'Tuesday',
            item: this.storeArr.products[this.subscriptionData.tuesday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.tuesday = mealItemList;
        }
        if (
          this.subscriptionData.wednesday &&
          this.subscriptionData.wednesday !== 'none' &&
          this.subscriptionData.wednesday !== ''
        ) {
          bookingDays.push('Wednesday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.wednesday].name
          );

          let mealItemList = {
            day: 'Wednesday',
            item: this.storeArr.products[this.subscriptionData.wednesday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.wednesday = mealItemList;
        }
        if (
          this.subscriptionData.thursday &&
          this.subscriptionData.thursday !== 'none' &&
          this.subscriptionData.thursday !== ''
        ) {
          bookingDays.push('Thursday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.thursday].name
          );

          let mealItemList = {
            day: 'Thursday',
            item: this.storeArr.products[this.subscriptionData.thursday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.thursday = mealItemList;
        }
        if (
          this.subscriptionData.friday &&
          this.subscriptionData.friday !== 'none' &&
          this.subscriptionData.friday !== ''
        ) {
          bookingDays.push('Friday');
          bookingMeals.push(
            this.storeArr.products[this.subscriptionData.friday].name
          );

          let mealItemList = {
            day: 'Friday',
            item: this.storeArr.products[this.subscriptionData.friday]
          };
          bookingMealsItems.push(mealItemList);
          this.subscriptionData.items.friday = mealItemList;
        }

        if (bookingDays.length === 0) {
          this.toastr.error('Please pick meals for 5-days Or Change the plan.');
          //    this.toastr.info("Picked days - " + bookingDays.join(","));
          this.spinner.hide();
          return;
        }

        if (bookingDays.length !== 5) {
          this.toastr.error('Please pick meals for 5-days Or Change the plan.');
          this.toastr.info('Picked days - ' + bookingDays.join(','));
          this.spinner.hide();
          return;
        }

        if (bookingDays.includes('Friday')) {
          if (
            !this.subscriptionData.friday_special ||
            this.subscriptionData.friday_special === ''
          ) {
            this.toastr.error('Please pick dessert or fruit bowl for friday.');
            this.toastr.info('Picked days - ' + bookingDays.join(','));
            this.spinner.hide();
            return;
          } else {
            if (
              this.subscriptionData.friday_special &&
              this.subscriptionData.friday_special !== 'none' &&
              this.subscriptionData.friday_special !== ''
            ) {
              //  bookingDays.push("Friday Special");
              bookingMeals.push(
                this.storeArr.products[this.subscriptionData.friday_special]
                  .name
              );

              let mealItemList = {
                day: 'Friday Special',
                item: this.storeArr.products[
                  this.subscriptionData.friday_special
                ]
              };
              bookingMealsItems.push(mealItemList);
              this.subscriptionData.items.friday_special = mealItemList;
            }
          }
        }

        break;
      }
      default:
    }

    this.subscriptionData.booking_days = bookingDays;
    this.subscriptionData.booking_meals = bookingMeals;
    this.subscriptionData.booking_meals_items = bookingMealsItems;
    this.subscriptionData.meals_menu = this.mealsMenuArr;
    this.subscriptionData.total = this.plansArr[
      this.subscriptionData.plan
    ].price;
    this.subscriptionData.plan_details = this.plansArr[
      this.subscriptionData.plan
    ];

    this.localStorage.setItem(
      'subscriptionData',
      JSON.stringify(this.subscriptionData)
    );
    this.spinner.hide();

    this.router.navigate(['/checkout']);
  }

  readMore() {
    const data = {
      store_id: environment.storeId
    };

    this.spinner.show();

    this.store.getsubscriptionSummary(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          var pickListModal = swal
            .fire({
              width: '100%',
              // title: '<strong><u>Njas Meal Plan Subscription:</u></strong>',
              html: res.message
                ? '<div class="text-left">' + res.message + '</div>'
                : 'N/A',
              confirmButtonColor: '#DD6B55',
              showCloseButton: true,
              showConfirmButton: false,
              showCancelButton: false,
              focusConfirm: true,
              confirmButtonText: '<i class="fa fa-download"></i> Download!',
              confirmButtonAriaLabel: 'Thumbs up, great!'
              // cancelButtonText:
              //   '<i class="fa fa-times"></i>',
              // cancelButtonAriaLabel: 'Close'
            })
            .then(isConfirm => {
              // console.log(isConfirm);

              if (isConfirm.value) {
                //   this.getPDF(orderDetails.order_id);
              } else {
                //  swal("Cancelled", "Your imaginary file is safe :)", "error");
              }
            });
          $('.swal2-container').css('background-color', '#fff');
          $('.swal2-container.in').css('background-color', '#fff');

          this.spinner.hide();
        } else {
          this.toastr.warning('Something went wrong!');
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }

  getStoreDetails() {
    const data = {
      store_id: environment.storeId
    };

    this.spinner.show();

    this.store.getStoreDetails(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          if (res.message) {
            this.storeArr = res.message;
            // this.subscriptionData.store = this.storeArr.store;

            Object.keys(this.storeArr.sub_categories).forEach(key => {
              if (
                this.storeArr.sub_categories[key].name.toLowerCase().trim() ===
                'monday'
              ) {
                this.mealsMenuArr.monday.category = this.storeArr.sub_categories[
                  key
                ];
                let productsArr: any = [];
                Object.keys(this.storeArr.products).forEach(k => {
                  if (
                    this.storeArr.products[k].sub_category_id ===
                    this.storeArr.sub_categories[key].id
                  ) {
                    productsArr.push(this.storeArr.products[k]);
                  }
                });
                this.mealsMenuArr.monday.products = productsArr;
              }

              if (
                this.storeArr.sub_categories[key].name.toLowerCase().trim() ===
                'tuesday'
              ) {
                this.mealsMenuArr.tuesday.category = this.storeArr.sub_categories[
                  key
                ];
                let productsArr: any = [];
                Object.keys(this.storeArr.products).forEach(k => {
                  if (
                    this.storeArr.products[k].sub_category_id ===
                    this.storeArr.sub_categories[key].id
                  ) {
                    productsArr.push(this.storeArr.products[k]);
                  }
                });
                this.mealsMenuArr.tuesday.products = productsArr;
              }

              if (
                this.storeArr.sub_categories[key].name.toLowerCase().trim() ===
                'wednesday'
              ) {
                this.mealsMenuArr.wednesday.category = this.storeArr.sub_categories[
                  key
                ];
                let productsArr: any = [];
                Object.keys(this.storeArr.products).forEach(k => {
                  if (
                    this.storeArr.products[k].sub_category_id ===
                    this.storeArr.sub_categories[key].id
                  ) {
                    productsArr.push(this.storeArr.products[k]);
                  }
                });
                this.mealsMenuArr.wednesday.products = productsArr;
              }

              if (
                this.storeArr.sub_categories[key].name.toLowerCase().trim() ===
                'thursday'
              ) {
                this.mealsMenuArr.thursday.category = this.storeArr.sub_categories[
                  key
                ];
                let productsArr: any = [];
                Object.keys(this.storeArr.products).forEach(k => {
                  if (
                    this.storeArr.products[k].sub_category_id ===
                    this.storeArr.sub_categories[key].id
                  ) {
                    productsArr.push(this.storeArr.products[k]);
                  }
                });
                this.mealsMenuArr.thursday.products = productsArr;
              }

              if (
                this.storeArr.sub_categories[key].name.toLowerCase().trim() ===
                'friday'
              ) {
                this.mealsMenuArr.friday.category = this.storeArr.sub_categories[
                  key
                ];
                let productsArr: any = [];
                Object.keys(this.storeArr.products).forEach(k => {
                  if (
                    this.storeArr.products[k].sub_category_id ===
                    this.storeArr.sub_categories[key].id
                  ) {
                    productsArr.push(this.storeArr.products[k]);
                  }
                });
                this.mealsMenuArr.friday.products = productsArr;
              }

              if (
                this.storeArr.sub_categories[key].name.toLowerCase().trim() ===
                'friday special'
              ) {
                this.mealsMenuArr.friday_special.category = this.storeArr.sub_categories[
                  key
                ];
                let productsArr: any = [];
                Object.keys(this.storeArr.products).forEach(k => {
                  if (
                    this.storeArr.products[k].sub_category_id ===
                    this.storeArr.sub_categories[key].id
                  ) {
                    productsArr.push(this.storeArr.products[k]);
                  }
                });
                this.mealsMenuArr.friday_special.products = productsArr;
              }
            });
            this.mealsMenuArr.menu_flag_count = 1;
          } else {
            this.storeArr = [];
          }

          this.spinner.hide();
        } else {
          this.storeArr = [];
          this.toastr.warning('Something went wrong!');
          return;
        }
      },
      (err: any) => {
        this.storeArr = [];
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}
