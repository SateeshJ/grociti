import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import * as $ from 'jquery';
import { WINDOW } from '@ng-toolkit/universal';
declare var jQuery: any;
declare var Swiper: any;
import { Title, Meta } from '@angular/platform-browser';

@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.scss']
})
export class JobDescriptionComponent implements OnInit {
  id: any;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService
  ) {}
  ngOnInit() {
    /*trying to eliminated # from url*/
    // console.log(this.router.url.split('/')[2]);

    if (this.router.url.split('/')[2]) {
      this.id = this.router.url.split('/')[2];
    }
    /*trying to eliminated # from url*/
  }
}
