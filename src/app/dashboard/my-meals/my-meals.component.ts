import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';
import { CurrencySymbolPipe } from '../../currency-symbol.pipe';
@Component({
  selector: 'app-my-meals',
  templateUrl: './my-meals.component.html',
  styleUrls: ['./my-meals.component.scss']
})
export class MyMealsComponent implements OnInit {
  customerMealsArr: any = [];
  ordersObj: any;
  ordersTotal = 0;
  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    public auth: AuthService
  ) {}
  ngOnInit() {
    this.getAllCustomerMealOrdersByEmailID();
  }
  getAllCustomerMealOrdersByEmailID() {
    const data = {
      store_name: environment.mealsStoreId
    };
    this.spinner.show();
    this.store.getAllCustomerMealOrdersByEmailID(data).subscribe(
      (res: any) => {
        if (res.status === 'success') {
          this.ordersObj = res.message;

          this.customerMealsArr = [];
          //this.ordersObj = Object["values"](res.message);

          this.ordersTotal = 0;

          Object.keys(this.ordersObj).forEach(key => {
            this.ordersTotal =
              this.ordersTotal + +this.ordersObj[key].order.payable;
            this.customerMealsArr.push(this.ordersObj[key]);
          });

          this.customerMealsArr.reverse();

          this.spinner.hide();
        } else {
          this.customerMealsArr = [];
          this.spinner.hide();
          return;
        }
      },
      (err: any) => {
        // console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}
