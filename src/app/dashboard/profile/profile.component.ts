import { NgxSpinnerService } from 'ngx-spinner';
import { StoreService } from '../../services/store.service';
import { environment } from '../../../environments/environment';
import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { AuthService } from '../../services/auth.service';
import { WindowService } from '../../services/window.service';
import firebase from 'firebase/app';
// import * as firebase from 'firebase/app';
require('firebase/auth');

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

// export class PhoneNumber {
//   country: string;
//   area: string;
//   prefix: string;
//   line: string;

//   // format phone numbers as E.164
//   get e164() {
//     const num = this.country + this.area + this.prefix + this.line
//     return `+${num}`
//   }

// }
export class ProfileComponent implements OnInit {
  windowRef: any;

  phoneNumber: any;

  verificationCode: string;

  user: any;
  public recaptchaVerifier: firebase.auth.RecaptchaVerifier;
  customerDetailsArr: any = [];

  firebaseConfig = {
    apiKey: 'AIzaSyD3Nk0bEjDEmJHOPeBz6hRlQJGQy4Bv1S8',
    authDomain: 'notjusasalad-df110.firebaseapp.com',
    databaseURL: 'https://notjusasalad-df110.firebaseio.com',
    projectId: 'notjusasalad-df110',
    storageBucket: 'notjusasalad-df110.appspot.com',
    messagingSenderId: '60519586840',
    appId: '1:60519586840:web:a23ddacb7406457dcc776d',
    measurementId: 'G-C1NQH5VTP8'
  };

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,

    private win: WindowService,
    private toastr: ToastrService,
    public router: Router,
    public store: StoreService,
    private spinner: NgxSpinnerService,
    public auth: AuthService
  ) {}
  ngOnInit() {
    this.customerDetailsArr = this.auth.user$;
    this.windowRef = this.win.windowRef;
    // this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container')
    // this.windowRef.recaptchaVerifier.render()
    if (!firebase.apps.length) {
      firebase.initializeApp(this.firebaseConfig);
    }
  }

  public submitForm(form: any) {
    if (!form.value) {
      this.toastr.warning('Please fill in the required details!');
      return;
    }

    if (form.value.mobile.trim() == '') {
      this.toastr.error('Please enter your contact number');
      this.spinner.hide();
      return false;
    } else {
      var phoneNum = form.value.mobile.trim().replace(/[^\d]/g, '');
      if (phoneNum.length > 6 && phoneNum.length < 12) {
      } else {
        this.toastr.error('Please enter valid contact number');
        this.spinner.hide();
        return false;
      }
    }

    if (form.value.secondary_mobile) {
      if (form.value.secondary_mobile.trim()) {
        var phoneNum = form.value.secondary_mobile.trim().replace(/[^\d]/g, '');
        if (phoneNum.length > 6 && phoneNum.length < 12) {
        } else {
          this.toastr.error('Please enter valid seconday contact number');
          this.spinner.hide();
          return false;
        }
      }
    } else {
      form.value.secondary_mobile = '';
    }

    this.spinner.show();
    let data = JSON.stringify({
      id: form.value.id,
      email: this.customerDetailsArr.email,
      name: form.value.name,
      mobile: form.value.mobile,
      secondary_mobile: form.value.secondary_mobile,
      address1: form.value.address1
    });

    this.auth.editMemberProfile(data).subscribe(
      (res: any) => {
        this.spinner.hide();
        if (res.status === 'success') {
          localStorage.setItem(
            'currentUserProfile',
            JSON.stringify(res.message)
          );
          this.auth.user$ = res.message;
          this.toastr.success('Profile edited!');
          // this.router.navigate(['/signin']);
        } else {
          //  this.auth.user$ = [];
          this.toastr.error(res.message);
          return;
        }
      },
      (err: any) => {
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }

  numberOnly(event: any): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  sendLoginCode(num: any) {
    const appVerifier = new firebase.auth.RecaptchaVerifier(
      'recaptcha-container',
      {
        size: 'invisible'
      }
    );
    //const num = this.phoneNumber.e164;

    num = '+1' + num;
    this.spinner.show();
    firebase
      .auth()
      .signInWithPhoneNumber(num, appVerifier)
      .then(result => {
        this.spinner.hide();
        this.windowRef.confirmationResult = result;
        // this.toastr.error();
        //alert("success");
        //console.log(result);
      })
      .catch(error => {
        this.spinner.hide();
        this.toastr.error('Invalid phone number!');
        // alert("fail");
        //console.log(error)
        // console.log(result);
      });
  }

  verifyLoginCode() {
    if (!this.verificationCode) {
      this.toastr.error('Enter OTP!');
      return;
    }

    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then((result: { user: any }) => {
        this.user = result.user;
        this.checkMobileVerification();
      })
      .catch((error: any) => {
        // console.log(error, "Incorrect code entered?")
        this.toastr.error('Invalid otp!');
      });
  }

  checkMobileVerification() {
    const data = {
      id: this.customerDetailsArr.id,
      mobile: this.customerDetailsArr.mobile
    };
    this.spinner.show();
    this.store.checkMobileVerification(data).subscribe(
      (res: any) => {
        //  $("#load").hide();

        if (res.status === 'success') {
          this.customerDetailsArr.mobile_verification = 'true';

          let userdata = JSON.parse(localStorage.getItem('currentUserProfile'));
          userdata.mobile = this.customerDetailsArr.mobile;
          userdata.mobile_verification = 'true';
          // this.auth.user$.mobile_verification = "true";
          localStorage.setItem('currentUserProfile', JSON.stringify(userdata));
          this.auth.user$ = userdata;

          this.spinner.hide();
        } else {
          this.toastr.warning(res.message);
          this.spinner.hide();
          return;
        }
      },
      (err: any) => {
        console.log(err.error);
        this.spinner.hide();
        this.toastr.warning('Something went wrong!');
      }
    );
  }
}
